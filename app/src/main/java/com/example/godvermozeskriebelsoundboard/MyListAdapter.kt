package com.example.godvermozeskriebelsoundboard

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat

class MyListAdapter(context: Context, dares: List<String>) :
    BaseAdapter() {

    private val mContext: Context
    private val mBtn: List<String>

    init {
        mContext = context
        mBtn = dares
    }

    //responsible for rendering out each row
    override fun getView(position: Int, convertView: View?, viewGroup: ViewGroup?): View {
        val layoutInflater = LayoutInflater.from(mContext)
        val rowMain = layoutInflater.inflate(R.layout.btn_list, viewGroup, false)
        val btnText = rowMain.findViewById<TextView>(R.id.btn)
        val btnbox = rowMain.findViewById<ConstraintLayout>(R.id.btnContainer)
        btnText.text = mBtn[position]


        if (position == 0) {
            btnbox.background = ContextCompat.getDrawable(mContext, R.drawable.orange_gradient)
            Log.d("colour", "orange".plus(mBtn[position].plus(position)))
        }
        if (position % 6 == 0) {
            btnbox.background = ContextCompat.getDrawable(mContext, R.drawable.orange_gradient)
            Log.d("colour", "orange".plus(mBtn[position].plus(position)))
        } else if (position % 5 == 0) {
            btnbox.background = ContextCompat.getDrawable(mContext, R.drawable.yellow_gradient)
            Log.d("colour", "yellow".plus(mBtn[position].plus(position)))
        } else if (position % 4 == 0) {
            btnbox.background = ContextCompat.getDrawable(mContext, R.drawable.purple_gradient)
            Log.d("colour", "purple".plus(mBtn[position].plus(position)))
        } else if (position % 3 == 0) {
            btnbox.background = ContextCompat.getDrawable(mContext, R.drawable.red_gradient)
            Log.d("colour", "red".plus(mBtn[position].plus(position)))
        } else if (position % 2 == 0) {
            btnbox.background = ContextCompat.getDrawable(mContext, R.drawable.green_gradient)
            Log.d("colour", "green".plus(mBtn[position].plus(position)))
        } else {
            btnbox.background = ContextCompat.getDrawable(mContext, R.drawable.blue_gradient)
            Log.d("colour", "blue".plus(mBtn[position].plus(position)))
        }
        return rowMain
    }

    //ignore this for now
    override fun getItem(position: Int): Any {
        return "Test string"
    }

    //ignore this for now
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    //responsible for how many rows in my list
    override fun getCount(): Int {
        return mBtn.size
    }
}

