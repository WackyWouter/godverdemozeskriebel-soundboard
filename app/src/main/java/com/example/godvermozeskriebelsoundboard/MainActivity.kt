package com.example.godvermozeskriebelsoundboard

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.hide()
        createList()
    }

    fun createList() {
        val listView = list
        val list = listOf("pen", "cup", "dog", "spectacles", "TEST", "hleleleo")
        listView.adapter = MyListAdapter(this, list)

//        listView.setOnItemClickListener { parent, view, position, id ->
//            val specificDare = arrayOf(dares[position])
//            val intent = Intent(this, DareDetails::class.java)
//            intent.putExtra("details", dares[position].id)
//            startActivity(intent)
//        }
    }
}
